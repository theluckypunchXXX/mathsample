object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Math Sample'
  ClientHeight = 260
  ClientWidth = 456
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object lblMsg: TLabel
    Left = 240
    Top = 65
    Width = 47
    Height = 13
    Caption = 'Messages'
  end
  object btnCalculate: TButton
    Left = 240
    Top = 14
    Width = 75
    Height = 25
    Caption = 'Calculate'
    TabOrder = 0
    OnClick = btnCalculateClick
  end
  object grpAdd: TGroupBox
    Left = 8
    Top = 8
    Width = 217
    Height = 57
    Caption = 'Addition'
    TabOrder = 1
    object lbl1: TLabel
      Left = 68
      Top = 23
      Width = 8
      Height = 13
      Caption = '+'
    end
    object Label1: TLabel
      Left = 141
      Top = 23
      Width = 8
      Height = 13
      Caption = '='
    end
    object eAdd1: TEdit
      Left = 16
      Top = 20
      Width = 41
      Height = 21
      Alignment = taCenter
      NumbersOnly = True
      TabOrder = 0
      Text = '0'
    end
    object eAdd2: TEdit
      Left = 88
      Top = 20
      Width = 41
      Height = 21
      Alignment = taCenter
      NumbersOnly = True
      TabOrder = 1
      Text = '0'
    end
    object eAddResult: TEdit
      Left = 160
      Top = 20
      Width = 41
      Height = 21
      Alignment = taCenter
      ReadOnly = True
      TabOrder = 2
      Text = '0'
    end
  end
  object grpSubtraction: TGroupBox
    Left = 8
    Top = 72
    Width = 217
    Height = 57
    Caption = 'Subtraction'
    TabOrder = 2
    object Label2: TLabel
      Left = 68
      Top = 23
      Width = 4
      Height = 13
      Caption = '-'
    end
    object Label3: TLabel
      Left = 141
      Top = 23
      Width = 8
      Height = 13
      Caption = '='
    end
    object eSub1: TEdit
      Left = 16
      Top = 20
      Width = 41
      Height = 21
      Alignment = taCenter
      NumbersOnly = True
      TabOrder = 0
      Text = '0'
    end
    object eSub2: TEdit
      Left = 88
      Top = 20
      Width = 41
      Height = 21
      Alignment = taCenter
      NumbersOnly = True
      TabOrder = 1
      Text = '0'
    end
    object eSubResult: TEdit
      Left = 160
      Top = 20
      Width = 41
      Height = 21
      Alignment = taCenter
      ReadOnly = True
      TabOrder = 2
      Text = '0'
    end
  end
  object grpMultiplication: TGroupBox
    Left = 8
    Top = 136
    Width = 217
    Height = 57
    Caption = 'Multiplication'
    TabOrder = 3
    object Label4: TLabel
      Left = 68
      Top = 23
      Width = 6
      Height = 13
      Caption = '*'
    end
    object Label5: TLabel
      Left = 141
      Top = 23
      Width = 8
      Height = 13
      Caption = '='
    end
    object eMul1: TEdit
      Left = 16
      Top = 20
      Width = 41
      Height = 21
      Alignment = taCenter
      NumbersOnly = True
      TabOrder = 0
      Text = '0'
    end
    object eMul2: TEdit
      Left = 88
      Top = 20
      Width = 41
      Height = 21
      Alignment = taCenter
      NumbersOnly = True
      TabOrder = 1
      Text = '0'
    end
    object eMulResult: TEdit
      Left = 160
      Top = 20
      Width = 41
      Height = 21
      Alignment = taCenter
      ReadOnly = True
      TabOrder = 2
      Text = '0'
    end
  end
  object grpDivision: TGroupBox
    Left = 8
    Top = 200
    Width = 217
    Height = 57
    Caption = 'Division'
    TabOrder = 4
    object Label6: TLabel
      Left = 68
      Top = 23
      Width = 4
      Height = 13
      Caption = ':'
    end
    object Label7: TLabel
      Left = 141
      Top = 23
      Width = 8
      Height = 13
      Caption = '='
    end
    object eDiv1: TEdit
      Left = 16
      Top = 20
      Width = 41
      Height = 21
      Alignment = taCenter
      NumbersOnly = True
      TabOrder = 0
      Text = '0'
    end
    object eDiv2: TEdit
      Left = 88
      Top = 20
      Width = 41
      Height = 21
      Alignment = taCenter
      NumbersOnly = True
      TabOrder = 1
      Text = '0'
    end
    object eDivResult: TEdit
      Left = 160
      Top = 20
      Width = 41
      Height = 21
      Alignment = taCenter
      ReadOnly = True
      TabOrder = 2
      Text = '0'
    end
  end
  object mmoMessage: TMemo
    Left = 231
    Top = 81
    Width = 201
    Height = 134
    TabOrder = 5
  end
  object btnClear: TButton
    Left = 357
    Top = 218
    Width = 75
    Height = 25
    Caption = 'Clear'
    TabOrder = 6
    OnClick = btnClearClick
  end
end
