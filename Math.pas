﻿{ ****************************************************************************************************************************
  Copyright (c) 2022 H�cker Automation GmbH. All rights reserved.
  https://haecker-automation.de
  **************************************************************************************************************************** }
unit Math;

interface

{$REGION 'uses'}

uses
  MathInterface,
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls;
{$ENDREGION}

type

  TForm1 = class(TForm)
    btnCalculate: TButton;
    grpAdd: TGroupBox;
    eAdd1: TEdit;
    lbl1: TLabel;
    eAdd2: TEdit;
    Label1: TLabel;
    eAddResult: TEdit;
    grpSubtraction: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    eSub1: TEdit;
    eSub2: TEdit;
    eSubResult: TEdit;
    grpMultiplication: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    eMul1: TEdit;
    eMul2: TEdit;
    eMulResult: TEdit;
    grpDivision: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    eDiv1: TEdit;
    eDiv2: TEdit;
    eDivResult: TEdit;
    mmoMessage: TMemo;
    btnClear: TButton;
    lblMsg: TLabel;
    procedure FormDestroy(Sender: TObject);
    procedure btnCalculateClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    fMathInterface: IMathInterface;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormDestroy(Sender: TObject);
begin
  fMathInterface := nil;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  fMathInterface := TMathInterface.create;

  eAdd1.Text := '2';
  eAdd2.Text := '3';
  eAddResult.Text := '';

  eSub1.Text := '7';
  eSub2.Text := '4';
  eSubResult.Text := '';

  eMul1.Text := '3';
  eMul2.Text := '4';
  eMulResult.Text := '';

  eDiv1.Text := '12';
  eDiv2.Text := '6';
  eDivResult.Text := '';

end;

{ TForm1 }

procedure TForm1.btnCalculateClick(Sender: TObject);
var
  vAddRes: Integer;
  vSubRes: Integer;
  vMulRes: Integer;
  vDivRes: Extended;
begin
  vAddRes := 0;
  vSubRes := 0;
  vMulRes := 0;
  vDivRes := 0.0;

  try
    vAddRes := fMathInterface.Adding(StrToInt(eAdd1.Text), StrToInt(eAdd2.Text));
    vSubRes := fMathInterface.Subtract(StrToInt(eSub1.Text), StrToInt(eSub2.Text));
    vMulRes := fMathInterface.Multiply(StrToInt(eMul1.Text), StrToInt(eMul2.Text));
    vDivRes := fMathInterface.Divide(StrToInt(eDiv1.Text), StrToInt(eDiv2.Text));
  except
    on E: Exception do
      mmoMessage.Lines.Add(E.Message);
  end;

  eAddResult.Text := IntToStr(vAddRes);
  eSubResult.Text := IntToStr(vSubRes);
  eMulResult.Text := IntToStr(vMulRes);
  eDivResult.Text := Format('%f', [vDivRes]);
end;

procedure TForm1.btnClearClick(Sender: TObject);
begin
  mmoMessage.Lines.Clear;
end;

end.
