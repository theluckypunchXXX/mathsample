unit test.MathInterface;

interface

uses
  MathInterface,
  DUnitX.TestFramework;

type

  [TestFixture]
  TMathTests = class
  strict private
    fMathInterfaceTest: IMathInterface;

  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [test]
    [TestCase('Test 1', '1,2,3')]
    [TestCase('Test 2', '0,9,9')]
    [TestCase('Test 3', '7,-1,6')]
    [TestCase('Test 3', '-5,-4, -9')]
    procedure Adding(const AValue1, AValue2, _Result: Integer);

    [test]
    [TestCase('Test 1', '5,2,3')]
    [TestCase('Test 2', '0,9,-9')]
    [TestCase('Test 3', '7,-2,9')]
    [TestCase('Test 3', '-2,-3, 1')]
    procedure Subtract(const AValue1, AValue2, _Result: Integer);

    [test]
    [TestCase('Test 1', '0,0,0')]
    [TestCase('Test 2', '0,1,0')]
    [TestCase('Test 3', '3,4,12')]
    [TestCase('Test 4', '-2,5,-10')]
    [TestCase('Test 5', '-4,-5, 20')]
    procedure Multiply(const AValue1, AValue2, _Result: Integer);

    [test]
    [TestCase('Test 1', '6,2,3')]
    [TestCase('Test 2', '7,4,1.75')]
    [TestCase('Test 3', '-5,2,-2.5')]
    [TestCase('Test 4', '-9,-3,3')]
    [TestCase('Test 5: with 0', '2,0,1')]
    procedure Divide(const AValue1, AValue2: Integer; _Result: Extended);
  end;

implementation

procedure TMathTests.Setup;
begin
  fMathInterfaceTest := TMathInterface.Create;
end;

procedure TMathTests.TearDown;
begin
  fMathInterfaceTest := nil;
end;

procedure TMathTests.Adding(const AValue1, AValue2, _Result: Integer);
var
  vRes: Integer;
begin
  vRes := fMathInterfaceTest.Adding(AValue1, AValue2);
  Assert.AreEqual(vRes, _Result);
end;

procedure TMathTests.Subtract(const AValue1, AValue2, _Result: Integer);
var
  vRes: Integer;
begin
  vRes := fMathInterfaceTest.Subtract(AValue1, AValue2);
  Assert.AreEqual(vRes, _Result);
end;

procedure TMathTests.Multiply(const AValue1, AValue2, _Result: Integer);
var
  vRes: Integer;
begin
  vRes := fMathInterfaceTest.Multiply(AValue1, AValue2);
  Assert.AreEqual(vRes, _Result);
end;

procedure TMathTests.Divide(const AValue1, AValue2: Integer; _Result: Extended);
var
  vRes: Extended;
begin
  if AValue2 <> 0 then
  begin
    vRes := fMathInterfaceTest.Divide(AValue1, AValue2);
    Assert.AreEqual(vRes, _Result);
  end
  else
  begin
  Assert.WillRaise(
    procedure
    begin
      fMathInterfaceTest.Divide(AValue1, AValue2);
    end);
  end;
end;


initialization

TDUnitX.RegisterTestFixture(TMathTests);

end.
