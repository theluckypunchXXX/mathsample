unit MathInterface;

interface

type
  IMathInterface = Interface
    function Adding(const aValue1, aValue2: Integer): Integer;
    function Subtract(const aValue1, aValue2: Integer): Integer;
    function Multiply(const aValue1, aValue2: Integer): Integer;
    function Divide(const aValue1, aValue2: Integer): Extended;
  End;

  TMathInterface = class(TInterfacedObject, IMathInterface)
  private
    // IMathInterface
    function Adding(const aValue1, aValue2: Integer): Integer;
    function Subtract(const aValue1, aValue2: Integer): Integer;
    function Multiply(const aValue1, aValue2: Integer): Integer;
    function Divide(const aValue1, aValue2: Integer): Extended;
  end;

implementation

uses
  System.SysUtils;

{ TMathInterface }

function TMathInterface.Adding(const aValue1, aValue2: Integer): Integer;
begin
  Result :=  aValue1 + aValue2;
end;

function TMathInterface.Subtract(const aValue1, aValue2: Integer): Integer;
begin
  Result :=  aValue1 - aValue2;
end;

function TMathInterface.Multiply(const aValue1, aValue2: Integer): Integer;
begin
  Result :=  aValue1 * aValue2;
end;

function TMathInterface.Divide(const aValue1, aValue2: Integer): Extended;
begin
  if aValue2 = 0 then
    raise Exception.Create('Division by Zero not allowed')
  else
    Result :=  aValue1 / aValue2;
end;



end.
