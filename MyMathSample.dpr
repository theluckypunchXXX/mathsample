program MyMathSample;

uses
  Vcl.Forms,
  Math in 'Math.pas' {Form1},
  MathInterface in 'Source\MathInterface.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
